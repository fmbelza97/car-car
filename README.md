# CarCar

Team:

* Francis Belza - Services microservice (and ModelForm, AutomobileList, AutomobileForm)
* Hamzah - Sales


## Design

## Service microservice

The service microservice requires three models - Technician, Appointment, and AutomobileVO. View functions will be created afterwards in order to create, read, update and/or delete their respective models. AutomobileVO will be used in the poller to retrieve automobile VINs and whether or not they've been sold from the inventory. React frontend will be built to display lists and creation forms.

## Sales microservice

The Sales microservice comprises four main components: Salesperson, AutomobileVO, Customer, and Sale models. We also have a sales poller dedicated to continuously fetching data. This data is then integrated with other microservices, including inventory, and stored in a PostgreSQL database, view the design breakdown below:


![Alt text](<CarCar Project Beta Design-1.png>)