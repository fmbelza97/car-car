import React, { useState, useEffect } from 'react';

function ServiceHistory() {
  const [appointments, setAppointments] = useState([]);
  const [searchVin, setSearchVin] = useState('');

  useEffect(() => {
    const fetchAppointments = async () => {
      const response = await fetch('http://localhost:8080/api/history/');
      if (response.ok) {
        const data = await response.json();
        setAppointments(data.appointments);
      }
    };

    fetchAppointments();
  }, []);

  const handleSearch = () => {
    const filtered = appointments.filter((appointment) =>
      appointment.vin.toLowerCase().includes(searchVin.toLowerCase())
    );
    setAppointments(filtered);
  };


  return (
    <div>
      <h1>Service History</h1>
      <div>
        <label htmlFor="vinInput">Search by VIN:</label>
        <input
          type="text"
          id="vinInput"
          value={searchVin}
          onChange={(e) => setSearchVin(e.target.value)}
        />
        <button type="button" className="btn btn-secondary" onClick={handleSearch}>Search</button>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Customer Name</th>
            <th>Date & Time</th>
            <th>Technician Name</th>
            <th>Reason</th>
            <th>Status</th>
            <th>VIP</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map((appointment) => (
            <tr key={appointment.id}>
              <td>{appointment.vin}</td>
              <td>{appointment.customer}</td>
              <td>{appointment.date_time}</td>
              <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
              <td>{appointment.reason}</td>
              <td>{appointment.status}</td>
              <td>{appointment.vip ? 'Yes' : 'No'}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ServiceHistory;
